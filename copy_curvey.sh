#!/bin/bash
# Script to copy a job templates survey from one to another
# If successful, final line of output will read "UPDATE 1"
# If output reads "UPDATE 0" then the job did not get copied.
# Make sure the job name is in quotes with proper case.

function usage {
echo "Usage:"
echo "./copy_survey.sh \"original job name\" \"destination job name\""
echo
}

if [ $# -ne 2 ]; then
        echo "Wrong number of arguments"
        echo
        usage
        exit
fi

JOB1=$(psql -t -U postgres -d awx -c "select name from main_unifiedjobtemplate where name = '$1';")
JOB2=$(psql -t -U postgres -d awx -c "select name from main_unifiedjobtemplate where name = '$2';")

if [ -z "$JOB1" ]; then
        echo "$1 not found"
        exit
else
        if [ -z "$JOB2" ]; then
                echo "$2 not found"
                exit
        fi
fi

echo Will copy \"$1\" to \"$2\"

DSURVEY=$(psql -t -U postgres -d awx -c "select survey_spec from main_jobtemplate where unifiedjobtemplate_ptr_id = (select id from main_unifiedjobtemplate where name = '$2');")

if [ -n "$DSURVEY" ]; then
	echo -n "Survey already exists. Overwrite? [y|n] "
	read answer
	if [ $answer == "y" ]; then
		psql -U postgres -d awx -c "update main_jobtemplate set survey_spec = \
		(select survey_spec from main_jobtemplate where unifiedjobtemplate_ptr_id = \
		(select id from main_unifiedjobtemplate where name = '$1')) \
		where unifiedjobtemplate_ptr_id = \
		(select id from main_unifiedjobtemplate where name = '$2');"
	else
		echo "Quitting"
		exit
	fi
else
        psql -U postgres -d awx -c "update main_jobtemplate set survey_spec = \
        (select survey_spec from main_jobtemplate where unifiedjobtemplate_ptr_id = \
        (select id from main_unifiedjobtemplate where name = '$1')) \
        where unifiedjobtemplate_ptr_id = \
        (select id from main_unifiedjobtemplate where name = '$2');"
fi
